package components

import "fmt"

type Paging struct {
	Page       int // current page from 1 to totalPages
	PerPage    int
	TotalPages int
}

// make pager in HTML
func (p Paging) MakePager() string {
	html := "<nav class='paging'>"

	var class string
	for i := 1; i <= p.TotalPages; i++ {
		class = ""
		if i == p.Page {
			class = "active"
		}
		html += fmt.Sprintf("<a href='?page=%d' class='paging-link %s'>%d</a>", i, class, i)
	}

	html += "</nav>"
	return html
}
