package components

import "fmt"

type SortOrder string

const (
	OrderASC  SortOrder = "ASC"
	OrderDESC SortOrder = "DESC"
)

type Sorting struct {
	Sort  string    // "column" name (request)
	Order SortOrder // direction
}

// label for UI, name for data
func (s Sorting) MakeSorter(label, name string) string {
	arrow := ""
	newOrder := string(OrderASC)

	if s.Sort == name {
		// default is sorted alpha
		arrow = " ⏶"
		newOrder = string(OrderDESC)
		if s.Order == OrderDESC {
			arrow = " ⏷"
			newOrder = string(OrderASC)
		}
	}

	return fmt.Sprintf("<a href='?sort=%s&order=%s' class='sorting-link'>%s %s</a>", name, newOrder, label, arrow)
}
