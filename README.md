# TODO
List of things to fix/change/improve.

## Sorting
- [_] Currently, sorting only works on `Name`` field.
- [_] Sorting columns works by comparing the label of the column.  
      Column name and label should be disassociated.