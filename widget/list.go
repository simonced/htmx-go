package widget

import (
	"htmx-go/components"
	d "htmx-go/data"
	"htmx-go/template"
	"math"
)

// generates a list
func List(filter string, page int, sorting components.Sorting) string {
	// sample, random data
	headers := template.TableHeader{
		"Name",
		"Age",
		"Email",
	}

	// paging setup
	perPage := 10 // default paging size TODO a conf or somethign
	paging := components.Paging{
		Page:       page,
		PerPage:    perPage,
		TotalPages: 0, // dont know yet
	}
	// filtered/asked data
	people, matchingCount := d.GetData(filter, paging, sorting)
	paging.TotalPages = int(math.Ceil(float64(matchingCount) / float64(perPage)))

	return template.PersonsTable(headers, people.IntoTable(), matchingCount, paging, sorting)
}
