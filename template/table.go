package template

import (
	"fmt"
	"htmx-go/components"
)

type TableHeader []string
type TableData [][]string

// makes a table from anything, and that's it
func Table(headers TableHeader, lines TableData, sorting components.Sorting) string {
	html := "<table>"

	// headers
	html += "<thead><tr>"
	for _, header := range headers {
		headerLink := sorting.MakeSorter(header, header) // FIXME
		html += "<th>" + headerLink + "</th>"
	}
	html += "</thead></tr>"

	// DATA
	html += "<tbody>"
	for _, line := range lines {
		html += "<tr>"
		for _, col := range line {
			html += "<td>" + col + "</td>"
		}
		html += "</tr>"
	}
	html += "</tbody>"

	html += "</table>"

	return html
}

// list of persons
func PersonsTable(headers TableHeader, lines TableData, total int, paging components.Paging, sorting components.Sorting) string {
	html := ""

	html += "<div class='table-before'>"
	html += fmt.Sprintf("<div>Results: %d / %d</div>", len(lines), total)
	html += paging.MakePager()
	html += "</div>" // table-before

	html += Table(headers, lines, sorting)
	return html
}
