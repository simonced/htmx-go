package template

import (
	"fmt"
	"os"
)

func templateFullName(templateName string) string {
	return "template/" + templateName
}

type TablePagingLink struct{}

func (l TablePagingLink) Link(p int) string {
	return fmt.Sprintf("?page=%d", p)
}

func Display(templateName string) string {
	templFullName := templateFullName(templateName)
	content, err := os.ReadFile(templFullName)
	if err != nil {
		msg := fmt.Sprintf("can't read template %s", templateName)
		panic(msg)
	}

	return string(content)
}
