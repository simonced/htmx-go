package data

type TableAble interface {
	IntoTable() [][]string
}
