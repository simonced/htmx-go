package data

import (
	"encoding/json"
	"fmt"
	"htmx-go/components"
	"htmx-go/template"
	"math"
	"os"
	"sort"
	"strings"
)

type Person struct {
	Name  string `json:"name"`
	Age   int    `json:"age"`
	Email string `json:"email"`
}

type PersonList []Person

// transforms a list of persons into a matrix of data
func (p PersonList) IntoTable() [][]string {
	data := make(template.TableData, 0)
	for _, person := range p {
		newLine := []string{
			person.Name,
			fmt.Sprint(person.Age),
			person.Email,
		}
		data = append(data, newLine)
	}
	return data
}

func loadData() PersonList {
	// for now, read a simple mock data json file
	jsonContent, err := os.ReadFile("data/MOCK_DATA.json")
	if err != nil {
		panic("No JSON file bro!")
	}

	data := make([]Person, 0)
	err = json.Unmarshal(jsonContent, &data)
	if err != nil {
		panic("bruh, JSON broke!\n" + err.Error())
	}

	return data
}

func CountData() int {
	items := loadData()

	return len(items)
}

// sample: fetching data (from DB or whatever)
func GetData(filter string, page components.Paging, sorting components.Sorting) (PersonList, int) {
	data := loadData()

	var filtered PersonList
	for _, p := range data {
		if filter == "" || strings.Contains(strings.ToLower(p.Name), strings.ToLower(filter)) {
			filtered = append(filtered, p)
		}
	}

	// SORTING >>>
	sort.Slice(filtered, func(a, b int) bool {
		// ASC
		if sorting.Order == components.OrderASC {
			return filtered[a].Name < filtered[b].Name // TODO use sort column, not Name only
		}
		// DESC
		return filtered[a].Name > filtered[b].Name // TODO use sort column, not Name only
	})
	// <<<

	// share that logic somehow? >>>
	matchingCount := len(filtered)

	if len(filtered) == 0 {
		return filtered, 0
	}

	from := (page.Page - 1) * page.PerPage
	to := from + page.PerPage
	to = int(math.Min(float64(to), float64(len(filtered))))
	// <<<

	return filtered[from:to], matchingCount
}
