package main

import (
	"fmt"
	"htmx-go/components"
	"htmx-go/template"
	"htmx-go/widget"
	"net/http"
	"strconv"
)

func homeHandler(w http.ResponseWriter, req *http.Request) {
	fmt.Fprint(w, template.Display("home.html"))
}

func widgetListHandler(w http.ResponseWriter, req *http.Request) {
	// input
	filter := req.FormValue("filter")
	page := 1 // default page
	if p, err := strconv.Atoi(req.FormValue("page")); err == nil {
		page = p
	}

	sorting := components.Sorting{
		Sort:  "Name",
		Order: components.OrderASC,
	}
	if s := req.FormValue("sort"); s != "" {
		sorting.Sort = s
	}
	if o := req.FormValue("order"); o != "" {
		sorting.Order = components.SortOrder(o)
	}

	// fmt.Println(sorting) // DBG
	// fmt.Println("filter with: ", filter) // DBG

	// return
	fmt.Fprint(w, widget.List(filter, page, sorting))
}

func main() {
	port := ":8888"
	http.HandleFunc("/", homeHandler)
	http.HandleFunc("/widget/list", widgetListHandler)
	fmt.Println("Bruh, it should be running on port ", port, "...")
	http.ListenAndServe(port, nil)
}
